FROM node:14.1.0-alpine3.11
RUN apk add --no-cache bash
RUN mkdir app
RUN mkdir setup
WORKDIR /setup
COPY create-react.sh .
RUN chmod +x create-react.sh
ENTRYPOINT ["./create-react.sh"]