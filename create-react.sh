#!/bin/bash

# Create the React app.
npx create-react-app $1

# Rename the README.md so we preserve the original README.md and copy everything to the app folder with the exception of node_modules folder.
cd $1
mv README.md README-REACT.md
shopt -s extglob
cp -r !(node_modules) /app
cd /app

# Only keep the original node-alpine version in the Dockerfile and add the lines we need for the usage of npm or yarn.
sed -i 2,8d Dockerfile

cat <<EOT >> Dockerfile
RUN mkdir app
WORKDIR /app
EOT

# Create a docker-compose.yml for using yarn or npm.
cat <<EOT >> docker-compose.yml
version: "3.3"
services:
    yarn:
        build:
            context: .
        entrypoint: yarn
        ports:
            - "3000:3000"
        volumes:
            - type: volume
              source: $1_node_modules
              target: /app/node_modules
            - type: bind
              source: .
              target: /app               
    npm:
        build:
            context: .
        entrypoint: npm
        ports:
            - "3000:3000"
        volumes:
            - type: volume
              source: $1_node_modules
              target: /app/node_modules
            - type: bind
              source: .
              target: /app
volumes:
    $1_node_modules:
EOT

# Remove this script from it's original location. We only need it once.
rm create-react.sh