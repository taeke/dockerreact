# DockerReact
A way to setup and develop https://reactjs.org/ in https://www.docker.com/ without even needing to install npm on your local machine. The only thing needed is Docker.

# Usage
Drop the Dockerfile and create-react.sh in a folder where you want to start developing a React application. Be sure to update the Node and or Alpine version in the Dockerfile if needed: see [Explanation](#explanation) why. To setup React and start it use the following in which you replace app-name with the name of your app of course:

```
docker build --tag create-react .
docker run --mount type=bind,source=$PWD,target=/app create-react app-name
docker-compose run yarn install
docker-compose run --service-ports yarn start
```

No the ```yarn install``` right after the ```create-react``` is not a mistake: see [Explanation](#explanation). After this you should be able to point your browser to http://localhost:3000/ and see the default React page. Editing the source should show you hot reloading in action. To understand the generated files once again: see [Explanation](#explanation).

If you want to run your Jest tests:
```
docker-compose run yarn test
```

If you need to add an extra Node package (for example https://material-ui.com/):
```
docker-compose run yarn add @material-ui/core
```

If you prefer to use npm:
```
docker build --tag create-react .
docker run --mount type=bind,source=$PWD,target=/app create-react app-name
docker-compose run npm install
docker-compose run npm install --package-lock-only
docker-compose run --service-ports npm start
```

If you put the generated files in a git repo any co-worker should be able to get going by cloning it and starting with:
```
docker-compose run yarn install
```

But someone could also develop it using npm/yarn on their local machine. As long as he or she leaves the docker-compose.yml in place they can develop any way they prefer and another still can use the Docker approach.

# Production build
Offcourse a ```docker-compose run yarn run build``` will create a local build. But you probably want to make a Docker image containing the build files. For instance you could create a multi-stage build in a build.Dockerfile to host it in https://www.nginx.com/:

```
FROM node:14.1.0-alpine3.11 as BUILD-STAGE
RUN mkdir app
WORKDIR /app
COPY ./package*.json ./yarn.lock  ./
RUN yarn install
COPY ./ ./
RUN CI=true yarn test
RUN yarn run build

FROM nginx:1.15
COPY --from=BUILD-STAGE /app/build/ /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
```

I named it build.Dockerfile so you can keep the original Dockerfile for the development. The advantage of using Dockerfile as extention instead of calling it DockerfileBuild or something like that is that atleast https://code.visualstudio.com/ still recognizes the file as a valid Dockerfile. 

# Explanation
## Update node version
I could probably get away with simply refering to latest, but https://www.docker.com/blog/intro-guide-to-dockerfile-best-practices/ convinced me to use a specific version. Be sure to pick a version that is compatible with the current React version. The same version will be used for the new Dockerfile which is updated by the create-react script and will be used by docker-compose. So there will be no nasty suprises with updated node versions later down the line.

## Files created
The first ```docker run --mount type=bind,source=$PWD,target=/app create-react app-name``` will create all the files needed for React on your local machine with the exception of the node_modules folder. It will also change the original Dockerfile and make it usefull for the docker-compose.yml file which is also created during this run. It will remove the create-react.sh file. You only need create-react.sh once so there is no need to keep this script and the original Dockerfile around, this README.md, if you copied it into the development folder, will still show the original content because there will be a README-REACT.md showing the content of the generated README.md file from React. After ```docker-compose run yarn install``` there will be a node_modules folder. Don't delete this node_modules folder even though it is empty: see [Docker named volume](#docker-named-volume) why.

## Docker named volume
The ```docker-compose run yarn install``` will create a docker named volume to use for the node_modules. You will also see a node_modules folder on your local machine. It will be empty but don't throw it away as it is your link with the named volume inside the Docker container. The content of the volume will be preserved, but if you mess it up somehow calling ```docker-compose run yarn install``` again will restore it. The named volume will get a unique name based on the app-name you provide. BE CAREFULL: do not create the same app-name twice on your machine it will probably mess up your Docker named volume.

## Why do we need yarn install after create-react?
The create-react script will create a node_modules folder and fill it, but it will only be inside the Docker container during the run. The Docker named volume needs to be unique and for that the app-name is used which is passed in the image when calling ```docker run``` on it. Consequently the name of the volume will only be known after starting the container or we should feed the run command the name of the app on two different locations: in an extra --mount for the name named volume and as an argument to the create-react script. I found it easier to do a ```docker-compose run yarn install``` afterwards and let it create and fill the Docker named volume because now the unique name of the volume is known in the generated docker-compose.yml.

# Motivation
I found a lot of links explaining how to use React in Docker for instance:

https://gist.github.com/przbadu/4a62a5fc5f117cda1ed5dc5409bd4ac1

https://dev.to/zivka51084113/dockerize-create-react-app-in-3-minutes-3om3

https://mherman.org/blog/dockerizing-a-react-app/

I learned a lot from them, but some do not use npx or needed to have npm installed on your local machine. So this is my take on how to use React in Docker.

# Dockerfile
```
FROM node:14.1.0-alpine3.11
RUN apk add --no-cache bash
RUN mkdir app
RUN mkdir setup
WORKDIR /setup
COPY create-react.sh .
RUN chmod +x create-react.sh
ENTRYPOINT ["./create-react.sh"]
```

# create-react.sh
```
#!/bin/bash

# Create the React app.
npx create-react-app $1

# Rename the README.md so we preserve the original README.md and copy everything to the app folder with the exception of node_modules folder.
cd $1
mv README.md README-REACT.md
shopt -s extglob
cp -r !(node_modules) /app
cd /app

# Only keep the original node-alpine version in the Dockerfile and add the lines we need for the usage of npm or yarn.
sed -i 2,8d Dockerfile

cat <<EOT >> Dockerfile
RUN mkdir app
WORKDIR /app
EOT

# Create a docker-compose.yml for using yarn or npm.
cat <<EOT >> docker-compose.yml
version: "3.3"
services:
    yarn:
        build:
            context: .
        entrypoint: yarn
        ports:
            - "3000:3000"
        volumes:
            - type: volume
              source: $1_node_modules
              target: /app/node_modules
            - type: bind
              source: .
              target: /app               
    npm:
        build:
            context: .
        entrypoint: npm
        ports:
            - "3000:3000"
        volumes:
            - type: volume
              source: $1_node_modules
              target: /app/node_modules
            - type: bind
              source: .
              target: /app
volumes:
    $1_node_modules:
EOT

# Remove this script from it's original location. We only need it once.
rm create-react.sh
```
